package de.walamana.pathfinding;

public class Node {

    public final int x, y, z;
    public boolean isObstacle;

    public boolean mazeMarked = false;

    private double g = Double.POSITIVE_INFINITY;
    private double h = Double.POSITIVE_INFINITY;

    public boolean closed = false;
    public Node previous = null;


    public Node(int x, int y, int z, boolean isObstacle){
        this.x = x;
        this.y = y;
        this.z = z;
        this.isObstacle = isObstacle;
    }

    public Node(int x, int y, int z){
        this(x, y, z, false);
    }



    public double getG() { return g; }
    public double getH() { return h; }
    public double getF() { return this.g + this.h; }

    public void setG(double g) { this.g = g; }
    public void setH(double h) { this.h = h; }

//    @Override
//    public String toString() {
//        return "Node{" +
//                "x=" + x +
//                ", y=" + y +
//                ", g=" + g +
//                ", h=" + h +
//                ", f=" + getF() +
//                ", closed=" + closed +
//                ", previous=" + previous +
//                '}';
//    }


    @Override
    public String toString() {
        return "[" + x + "|" + y + "|" + z + "]";
    }
}
