package de.walamana.pathfinding;

import com.sudoplay.joise.module.ModuleBasisFunction;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Board {

    public List<Node> list = new ArrayList<>();
    public int width = 0;
    public int height = 0;
    public boolean isWeighted = false;

    private Board(int width, int height, boolean isWeighted){
        this.width = width;
        this.height = height;
        this.isWeighted = isWeighted;
    }

    public static Board emptyBoard(int width, int height, boolean isWeighted){
        Board board = new Board(width, height, isWeighted);

        ModuleBasisFunction basis = new ModuleBasisFunction();
        basis.setType(ModuleBasisFunction.BasisType.SIMPLEX);
        basis.setSeed((long) (Math.random() * 10000));

        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                board.list.add(new Node(x, y, isWeighted ? (int) (Math.floor((basis.get(x / 7.0, y / 7.0) * 2) + 2)) : 1));
            }
        }

        return board;
    }


    /**
     * Generiert ein Brett mit zufällig platzierten Hindernissen
     * @param width
     * @param height
     * @param percentage Anteil der Hindernisse zum gesammten Brett
     */
    public static Board randomObstacles(int width, int height, boolean isWeighted, double percentage){
        Board board = Board.emptyBoard(width, height, isWeighted);

        for(int i = 0; i < board.list.size() * percentage; i++) {
            Node node = null;
            while(node == null || node.isObstacle){
                node = board.list.get((int) (Math.random() * board.list.size()));
            }
            node.isObstacle = true;
        }
        return board;
    }


    /**
     * Generiert ein Brett mit zufällig platzierten Hindernissen (Kreise)
     * @param width
     * @param height
     */
    public static Board randomObstacles(int width, int height, boolean isWeighted, int amount, int minRadius, int maxRadius){
        Board board = emptyBoard(width, height, isWeighted);

        for(int i = 0; i < amount; i++){
            int radius = (int) ((Math.random() * (maxRadius - minRadius)) + minRadius);
            int posX = (int) (Math.random() * width);
            int posY = (int) (Math.random() * height);

            for(int x = posX - radius; x <= posX + radius; x++){
                for(int y = posY - radius; y <= posY + radius; y++){
                    double distance = Math.sqrt(Math.pow(x - posX, 2) + Math.pow(y - posY, 2));
                    if(distance <= radius + 0.3 && board.getNode(x, y) != null){
                        board.getNode(x, y).isObstacle = true;
                    }
                }
            }

        }

        return board;
    }


    /**
     * Generiert ein Brett mit einem zufälligen Labyrinth
     * @param width
     * @param height
     * @param startX
     * @param startY
     * @param endX
     * @param endY
     * @return
     */
    public static Board randomMaze(int width, int height, boolean isWeighted, int startX, int startY, int endX, int endY){
        Board board = Board.emptyBoard(width, height, isWeighted);

        mazeNext(board, board.getNode(startX, startY), 1, board.getNode(endX, endY));

        return board;
    }

    // TODO: rename directionComingFrom
    private static void mazeNext(Board board, Node current, int directionComingFrom, Node end){
        current.mazeMarked = true;
        ArrayList<Integer> directions = new ArrayList<>(4);
        directions.add(0);
        directions.add(1);
        directions.add(2);
        directions.add(3);

        for(int i = 0; i < 4; i++){
            int direction = directions.get((int) (Math.random() * directions.size()));
            directions.remove((Object) direction);

            if((direction == 0 && directionComingFrom == 1)
            || (direction == 1 && directionComingFrom == 0)
            || (direction == 2 && directionComingFrom == 3)
            || (direction == 3 && directionComingFrom == 2)) continue;


            Node next = board.getNode(
                    current.x + (direction == 0 ? -1 : (direction == 1 ? 1 : 0)),
                    current.y + (direction == 2 ? -1 : (direction == 3 ? 1 : 0))
            );


            if(next == null || next.isObstacle || next == end || next.mazeMarked){
                continue; // FIXME: CHECK IF MAKES SENSE
            }

            switch (direction){
                case 0: case 1: {   // LEFT - RIGHT
                    Node bottom = board.getNode(current.x, current.y + 1);
                    Node top = board.getNode(current.x, current.y - 1);
                    if(bottom != null && !bottom.mazeMarked) bottom.isObstacle = true;
                    if(top != null && !top.mazeMarked) top.isObstacle = true;
                    break;
                }
                case 2: case 3: {   // UP - DOWN
                    Node left = board.getNode(current.x - 1, current.y);
                    Node right = board.getNode(current.x + 1, current.y);
                    if(right != null && !right.mazeMarked) right.isObstacle = true;
                    if(left != null && !left.mazeMarked) left.isObstacle = true;
                    break;
                }
            }
            mazeNext(board, next, direction, end);

        }
    }

    public Node getNode(int x, int y){
        if(x < 0 ||  x >= width || y < 0 || y >= height) return null;
        return list.get((y * width) + x);
    }

    public void reset(){
        for(Node n : list){
            n.closed = false;
            n.previous = null;
            n.setG(Double.POSITIVE_INFINITY);
            n.setH(Double.POSITIVE_INFINITY);
        }
    }

    public void print(){
        print(null, true);
    }

    public void print(Result result, boolean showElevation){
        String str = "";
        for(int y = 0; y < height; y++){
            for(int x = 0; x < width; x++){
                Node node = getNode(x, y);
                if(node.isObstacle){
                    str += "\033[1;100m[ ]\033[0m";
                }else if(result != null && result.path != null && result.path.contains(node)){
                    if(result.path.get(0) == node){
                        str += "\033[1;94m[" + node.z + "]\033[0m"; // Start ist blau
                    }else if(result.path.get(result.path.size() - 1) == node){
                        str += "\033[1;91m[" + node.z + "]\033[0m"; // Ziel ist rot
                    }else{
                        str += "\033[1;93m[" + node.z + "]\033[0m"; // Ansonsten gelb
                    }
                }else{
                    if(showElevation){
                        str += "[" + node.z + "]";
                    }else{
                        str += "[ ]";
                    }
                }
            }
            str += "\n";
        }
        System.out.println(str);
    }
}
