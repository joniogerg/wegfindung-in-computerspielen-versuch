package de.walamana.pathfinding;

import java.util.List;

public class Result {

    public List<Node> path;
    public long timeInNano;
    public long timeBacktraceInNano;

    public int maxListSize = 0;


    public Result(List<Node> path, long timeInNano, long timeBacktraceInNano, int maxListSize) {
        this.path = path;
        this.timeInNano = timeInNano;
        this.timeBacktraceInNano = timeBacktraceInNano;
        this.maxListSize = maxListSize;
    }

    public boolean failed(){ return path == null; }

    public double getLength(){
        double length = 0;
        Node prev = null;
        if(failed()) return 0;
        for(Node node : path){
            if(prev != null) {
                double h = node.z - prev.z;
                if(h == 0){
                    length += 1;
                }else{
                    length += Math.sqrt(h*h + 1);
                }
            }
            prev = node;
        }
        return length;
    }

    @Override
    public String toString() {
        return "Result{" +
                "path=" + path +
                ", timeInNano=" + timeInNano +
                ", timeBacktraceInNano=" + timeBacktraceInNano +
                ", maxListSize=" + maxListSize +
                '}';
    }

    public String toCSV(){
        return timeInNano + "," + timeBacktraceInNano + "," + maxListSize + "," + failed() + "," + getLength() + "," + pathToCSV();
    }

    private String pathToCSV(){
        if(failed()) return "{}";
        StringBuilder s = new StringBuilder("{");
        for(Node n : path){
            s.append("[").append(n.x).append(":").append(n.y).append(":").append(n.z).append(":").append(n.isObstacle).append("]");
        }
        s.append("}");
        return s.toString();
    }

    public static String header(){
        return "timeInNano,timeBacktraceInNano,maxListSize,failed,length,path";
    }

}
