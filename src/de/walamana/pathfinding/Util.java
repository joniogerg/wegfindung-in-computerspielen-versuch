package de.walamana.pathfinding;

import java.util.LinkedHashMap;
import java.util.List;

public class Util {

    public static long getAverageTime(List<Result> results){
        long averageTime = 0;
        for(Result r : results){
            averageTime += r.timeInNano;
        }
        return averageTime;
    }
}

