package de.walamana.pathfinding.heuristic;

import de.walamana.pathfinding.Node;

public class Heuristics {

    /**
     * Manhattan distance:
     * Sum of the distance between the x and the y values
     */
    public static Heuristic MANHATTAN = (from, to) -> Math.abs(to.x - from.x) + Math.abs(to.y - from.y);

    /**
     * Euclidean distance:
     * Diagonal distance between the two nodes
     */
    public static Heuristic EUCLIDEAN = (from, to) -> Math.sqrt( Math.pow(to.x - from.x, 2) + Math.pow(to.y - from.y, 2));

}
