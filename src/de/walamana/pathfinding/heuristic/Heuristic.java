package de.walamana.pathfinding.heuristic;

import de.walamana.pathfinding.Node;

public interface Heuristic {
    public double heuristic(Node from, Node to);
}
