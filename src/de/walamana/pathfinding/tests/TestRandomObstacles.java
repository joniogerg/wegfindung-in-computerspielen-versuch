package de.walamana.pathfinding.tests;

import de.walamana.pathfinding.Board;
import de.walamana.pathfinding.Result;
import de.walamana.pathfinding.Test;
import de.walamana.pathfinding.algorithms.AStarAlgorithm;
import de.walamana.pathfinding.algorithms.BeamSearch;
import de.walamana.pathfinding.algorithms.DynamicWeightAStarAlgorithm;
import de.walamana.pathfinding.heuristic.Heuristic;
import de.walamana.pathfinding.heuristic.Heuristics;

import java.lang.reflect.Method;
import java.util.LinkedHashMap;
import java.util.Map;

public class TestRandomObstacles extends Test {

    ResultSet aStar = new ResultSet();
    ResultSet beam = new ResultSet();
    ResultSet beamAggr = new ResultSet();
    ResultSet dyn = new ResultSet();

    int[] sizes = new int[]{20, 40, 100, 200};

    public TestRandomObstacles(String name, int maxBuffer) {
        super(name, maxBuffer);
    }

    @Override
    public void start() {
        super.start();
        int minRadius = 1;
        int maxRadius = 3;
        for(int s = 0; s < sizes.length; s++){
            System.out.println("\n#################################");
            System.out.println("Starting test s" + sizes[s] + "");
            int amount = (int) (sizes[s] * 0.5);
            for(int i = 0; i < 10; i++){
                Board board = Board.randomObstacles(sizes[s], sizes[s], false, amount, minRadius, maxRadius);
                aStar.add(run(board, new AStarAlgorithm(board)));
                beam.add(run(board, new BeamSearch(board, 100)));
                beamAggr.add(run(board, new BeamSearch(board, 100, BeamSearch.Method.AGGRESSIVE)));
                dyn.add(run(board, new DynamicWeightAStarAlgorithm(board)));
            }

            StringBuilder builder = new StringBuilder();
            builder.append(ResultSet.header() + "\n");
            builder.append(aStar.toString() + "\n");
            builder.append(beam.toString() + "\n");
            builder.append(beamAggr.toString() + "\n");
            builder.append(dyn.toString() + "\n");

            // Test summary
            saveTest(builder.toString(), "s" + sizes[s]);
            // Test results

            saveResults(aStar.getResultsCSV(), "s" + sizes[s] + "_astar_results");
            saveResults(beam.getResultsCSV(), "s" + sizes[s] + "_beam_results");
            saveResults(beamAggr.getResultsCSV(), "s" + sizes[s] + "_beamAggr_results");
            saveResults(dyn.getResultsCSV(), "s" + sizes[s] + "_dyn_results");

            System.out.println(aStar.getAverageTime() + "ms; " + aStar.getAverageTimeSucceeded() + "ms; " + aStar.getAverageTimeFailed() + "ms " + aStar.getAverageLength());
            System.out.println(beam.getAverageTime() + "ms; " + beam.getAverageTimeSucceeded() + "ms; " + beam.getAverageTimeFailed() + "ms " + beam.getAverageLength());
            System.out.println(beamAggr.getAverageTime() + "ms; " + beamAggr.getAverageTimeSucceeded() + "ms; " + beamAggr.getAverageTimeFailed() + "ms " + beamAggr.getAverageLength());
            System.out.println(dyn.getAverageTime() + "ms; " + dyn.getAverageTimeSucceeded() + "ms; " + dyn.getAverageTimeFailed() + "ms " + dyn.getAverageLength());

            aStar = new ResultSet();
            beam = new ResultSet();
            beamAggr = new ResultSet();
            dyn = new ResultSet();
        }
    }

    private Result run(Board board, AStarAlgorithm algorithm){
        int startX = 0;
        int startY = 0;
        int endX = board.width - 1;
        int endY = board.height - 1;

        Result r = algorithm.startSearch(board.getNode(startX, startY), board.getNode(endX, endY));
        board.reset();
        return r;
    }

    private void saveResults(){

    }
}
