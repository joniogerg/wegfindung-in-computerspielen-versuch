package de.walamana.pathfinding.algorithms;

import de.walamana.pathfinding.Board;
import de.walamana.pathfinding.Node;
import de.walamana.pathfinding.Result;
import de.walamana.pathfinding.heuristic.Heuristic;
import de.walamana.pathfinding.heuristic.Heuristics;

import java.util.*;

public class AStarAlgorithm {

    private Board board;
    protected PriorityQueue<Node> openList = new PriorityQueue<>(Comparator.comparingDouble(Node::getF));
    protected List<Node> closedList = new ArrayList<>();

    protected Node start = null;
    protected Node end = null;

    private long startTime = 0L;
    private long endTime = 0L;
    private long endTimeBacktrace = 0L;

    private Heuristic heuristic = null;

    private boolean invalid = false;

    private final static double SQRT2 = Math.sqrt(2);

    private List<Node> path;
    private int maxListSize = 0;

    public AStarAlgorithm(Board board, Heuristic heuristic){
        this.board = board;
        this.heuristic = heuristic;
    }

    public AStarAlgorithm(Board board){ this(board, Heuristics.MANHATTAN); }

    public Result startSearch(Node start, Node end){
        openList.add(start);
        start.isObstacle = false;
        end.isObstacle = false;

        this.start = start;
        this.end = end;

        this.start.setG(0);
        this.start.setH(heuristic(this.start, this.end));

        startTime = System.nanoTime();

        while(path == null && !invalid){
            search();
        }

//        System.out.println(this.path);
//        System.out.println(this.path.size() - 1);

        return new Result(this.path, this.endTime - this.startTime, this.endTimeBacktrace - this.startTime, maxListSize);
    }


    protected void search(){
        if(openList.size() > maxListSize) maxListSize = openList.size();
        if(openList.size() == 0){
            this.endTime = System.nanoTime();
            invalid = true;
            return;
        }

//        for(Node n : openList){
//            System.out.print(n.toString() + n.getF() +  ", ");
//        }
        Node cur = openList.poll();
//        System.out.println(":::" + cur);
        if(cur == end){
            // Trace back
            this.endTime = System.nanoTime();
            List<Node> path = new ArrayList<>();
            path.add(end);
            Node prev = end;
            while((prev = prev.previous) != null){
                path.add(prev);
            }
            Collections.reverse(path);
            this.endTimeBacktrace = System.nanoTime();
            this.path = path;
            return;
        }
        cur.closed = true;
        closedList.add(cur);

        for(Node neighbour : getNeighbourNodes(cur)){
            if(neighbour == null) continue;
            if(neighbour.closed) continue;
            if(neighbour.isObstacle) continue;


            double g = 1;
            if(board.isWeighted){
                /*
                 * Wenn es nach unten geht -> Wegkosten = 0.7 * Höhenunterschied
                 *
                 */
                int dif = neighbour.z - cur.z;
                double multiplier = dif < 0 ? 0.7 : 1;
                g = cur.getG() + dif * multiplier;
            }else{
                //                  Wenn es kein diagonaler Sprung ist
                g = cur.getG() + (cur.x - neighbour.x == 0 || cur.y - neighbour.y == 0 ? 1 : SQRT2);
            }
            if(!openList.contains(neighbour) || g < neighbour.getG()){
                neighbour.setG(g);
                neighbour.setH(heuristic(start, end));
                neighbour.previous = cur;

                if(!openList.contains(neighbour)) openList.add(neighbour);
            }
        }
    }

    protected double heuristic(Node start, Node end){
        return this.heuristic.heuristic(start, end);
    }

    private Node[] getNeighbourNodes(Node node){
        Node[] nodes = new Node[8];

        /* Top    */   nodes[0] = board.getNode(node.x, node.y - 1);
        /* Bottom */   nodes[1] = board.getNode(node.x, node.y + 1);
        /* Right  */   nodes[2] = board.getNode(node.x + 1, node.y);
        /* Left   */   nodes[3] = board.getNode(node.x - 1, node.y);

        if(!board.isWeighted){

            // DIAGONALE
            /* Top Left     */   if(nodes[0] != null && nodes[3] != null && !(nodes[0].isObstacle && nodes[3].isObstacle)) nodes[4] = board.getNode(node.x - 1, node.y - 1);
            /* Top Right    */   if(nodes[0] != null && nodes[2] != null && !(nodes[0].isObstacle && nodes[2].isObstacle)) nodes[5] = board.getNode(node.x + 1, node.y - 1);
            /* Bottom Left  */   if(nodes[1] != null && nodes[3] != null && !(nodes[1].isObstacle && nodes[3].isObstacle)) nodes[6] = board.getNode(node.x - 1, node.y + 1);
            /* Bottom Right */   if(nodes[1] != null && nodes[2] != null && !(nodes[1].isObstacle && nodes[2].isObstacle)) nodes[7] = board.getNode(node.x + 1, node.y + 1);

        }


        return nodes;
    }

}
