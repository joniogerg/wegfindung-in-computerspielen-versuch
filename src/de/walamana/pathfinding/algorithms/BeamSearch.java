package de.walamana.pathfinding.algorithms;

import de.walamana.pathfinding.Board;
import de.walamana.pathfinding.Node;
import de.walamana.pathfinding.heuristic.Heuristic;
import de.walamana.pathfinding.heuristic.Heuristics;

public class BeamSearch extends AStarAlgorithm {

    int maxSize = 5;
    Method method;

    public BeamSearch(Board board, Heuristic heuristic, int maxSize, Method method) {
        super(board, heuristic);
        this.maxSize = maxSize;
        this.method = method;
    }

    public BeamSearch(Board board, int maxSize, Method method) {
        this(board, Heuristics.MANHATTAN, maxSize, method);
    }

    public BeamSearch(Board board, int maxSize) {
        this(board, Heuristics.MANHATTAN, maxSize, Method.SOFT);
    }

    @Override
    protected void search() {
        while(openList.size() > maxSize){
            Node worst = openList.poll();
            worst.closed = true;
            closedList.add(worst);
            if(method == Method.SOFT) break;
        }
        super.search();
    }

    public static enum Method{
        /**
         * Entfernt nur das schlechteste Element aus der openList, wenn die Länge der Liste die definierte Maximallänge überschreitet
         */
        SOFT,

        /**
         * Entfernt so viele Knoten aus der openList, bis die Länge der openList der definierten Maximallänge entspricht
         */
        AGGRESSIVE
    }
}
