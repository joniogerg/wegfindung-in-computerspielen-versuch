package de.walamana.pathfinding.algorithms;

import de.walamana.pathfinding.Board;
import de.walamana.pathfinding.Node;
import de.walamana.pathfinding.Result;
import de.walamana.pathfinding.heuristic.Heuristic;

public class DynamicWeightAStarAlgorithm extends AStarAlgorithm{

    private double maxHeuristic;

    public DynamicWeightAStarAlgorithm(Board board, Heuristic heuristic, double maxHeuristic) {
        super(board, heuristic);
        this.maxHeuristic = maxHeuristic;
    }

    public DynamicWeightAStarAlgorithm(Board board) {
        super(board);
    }

    @Override
    public Result startSearch(Node start, Node end) {
        maxHeuristic = super.heuristic(start, end);
        return super.startSearch(start, end);
    }

    @Override
    protected double heuristic(Node start, Node end) {
        double h = super.heuristic(start, end);
        return h * Math.pow(0.02, h/maxHeuristic);
    }
}
