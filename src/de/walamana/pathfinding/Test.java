package de.walamana.pathfinding;

import de.walamana.pathfinding.algorithms.AStarAlgorithm;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public abstract class Test {

    public static final SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd_HH_mm_ss");

    public String name;
    public int maxBuffer;
    public Date startTime;

    public Test(String name, int maxBuffer){
        this(name, maxBuffer, new Date());
    }

    public Test(String name, int maxBuffer, Date startTime){
        this.name = name;
        this.maxBuffer = maxBuffer;
        this.startTime = startTime;
    }

    public void start(){
        System.out.println("Starting test \"" + this.name + "\" at " + new Date().toString());
    }

    public void saveTest(String data, String parameters){
        File dir = new File(Main.getExecutionPath(), "/results/" + format.format(startTime) + "/" + name +"/");
        if(!dir.exists()) dir.mkdirs();
        try {
            File file = new File(dir, name + "_" + parameters + ".csv");
            FileWriter writer = new FileWriter(file);
            writer.write(data);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    public void saveResults(String data, String parameters){
        File dir = new File(Main.getExecutionPath(), "/results/" + format.format(startTime) + "/" + name + "/results/");
        if(!dir.exists()) dir.mkdirs();
        try {
            File file = new File(dir, name + "_" + parameters + ".csv");
            FileWriter writer = new FileWriter(file);
            writer.write(data);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static class ResultSet{

        public ArrayList<Result> results = new ArrayList<>();

        public long averageTime = 0;
        public long failed = 0;
        public long averageTimeFailed = 0;
        public long averageTimeSucceeded = 0;

        public long averageTimeBacktrace = 0;
        public long averageTimeBacktraceFailed = 0;
        public long averageTimeBacktraceSucceeded = 0;

        public int testAmount = 0;

        public int averageMaxListSize = 0;

        public double length = 0;

        public void add(Result r){
            results.add(r);
            testAmount++;
            averageTime += r.timeInNano;
            averageTimeBacktrace += r.timeBacktraceInNano;
            averageMaxListSize += r.maxListSize;
            if(r.failed()){
                failed++;
                averageTimeFailed += r.timeInNano;
                averageTimeBacktraceFailed += r.timeBacktraceInNano;
            }else{
                averageTimeSucceeded += r.timeInNano;
                averageTimeBacktraceSucceeded += r.timeBacktraceInNano;
                length += r.getLength();
            }
        }


        public double getAverageTime() { return this.getAverageTime(true); }
        public double getAverageTime(boolean inMillis) {
            return (averageTime / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public long getFailed() {
            return failed;
        }

        public double getAverageTimeFailed() { return this.getAverageTimeFailed(true); }
        public double getAverageTimeFailed(boolean inMillis) {
            return (averageTimeFailed / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public double getAverageTimeSucceeded() { return this.getAverageTimeSucceeded(true); }
        public double getAverageTimeSucceeded(boolean inMillis) {
            return (averageTimeSucceeded / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public double getAverageTimeBacktrace() { return this.getAverageTimeBacktrace(true); }
        public double getAverageTimeBacktrace(boolean inMillis) {
            return (averageTimeBacktrace / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public double getAverageTimeBacktraceFailed() { return this.getAverageTimeBacktraceFailed(true); }
        public double getAverageTimeBacktraceFailed(boolean inMillis) {
            return (averageTimeBacktraceFailed / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public double getAverageTimeBacktraceSucceeded() { return this.getAverageTimeBacktraceSucceeded(true); }
        public double getAverageTimeBacktraceSucceeded(boolean inMillis) {
            return (averageTimeBacktraceSucceeded / (inMillis ? 1000000.0 : 1.0)) / testAmount;
        }

        public double getAverageLength(){
            return (length / (testAmount - failed));
        }

        public int getAverageMaxListSize() {
            return averageMaxListSize / testAmount;
        }

        public int getTestAmount() {
            return testAmount;
        }


        @Override
        public String toString() {
            return getAverageTime(true) + "," +
                    getAverageTimeBacktrace(true) + "," +
                    failed + "," +
                    getAverageTimeFailed(true) + ","+
                    getAverageTimeBacktraceFailed(true) + "," +
                    getAverageTimeSucceeded(true) + "," +
                    getAverageTimeBacktraceSucceeded(true) + "," +
                    getAverageMaxListSize() + "," +
                    testAmount + "," +
                    getAverageLength();
        }

        public String getResultsCSV(){
            StringBuilder s = new StringBuilder(Result.header());
            s.append("\n");
            for(Result r : results){
                s.append(r.toCSV()).append("\n");
            }
            return s.toString();
        }

        public static String header(){
            return "averageTime,averageTimeBacktrace,failed,averageTimeFailed,averageBacktraceFailed,averageSucceeded,averageBacktraceSucceeded,averageMaxListSize,testAmount,averageLength";
        }
    }

}
