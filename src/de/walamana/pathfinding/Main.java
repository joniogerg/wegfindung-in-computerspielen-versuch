package de.walamana.pathfinding;

import com.sudoplay.joise.module.ModuleBasisFunction;
import com.sudoplay.joise.module.ModuleScaleDomain;
import de.walamana.pathfinding.algorithms.AStarAlgorithm;
import de.walamana.pathfinding.algorithms.BeamSearch;
import de.walamana.pathfinding.algorithms.DynamicWeightAStarAlgorithm;
import de.walamana.pathfinding.tests.TestRandomObstacles;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public class Main {

    public static void main(String[] args) {
//        runMazeBoardTest();

//        runObstacleBoardTest();

        TestRandomObstacles testRandomObstacles = new TestRandomObstacles("test2", 1000);
        testRandomObstacles.start();
//        int sizeX = 10;
//        int sizeY = 10;
//        int startX = 0;
//        int startY = 0;
//        int endX = sizeX - 1;
//        int endY = sizeY - 1;
//        Board board = Board.emptyBoard(sizeX, sizeY, true);
//        Result r1 = new AStarAlgorithm(board).startSearch(board.getNode(startX, startY), board.getNode(endX, endY));
//        System.out.println("r1 Average time: " + (r1.timeInNano / 1000000D) + "ms");
//        System.out.println("r1 Average timeBacktrace: " + (r1.timeBacktraceInNano / 1000000D) + "ms");
//        System.out.println("Length: " + r1.getLength() + " Steps: " + r1.path.size());
//        board.print(r1, true);

    }

    private static void runMazeBoardTest(){
        int startX = 0;
        int startY = 0;
        int endX = 19;
        int endY = 18;

        Result longResult;
        while(true){
            Board board = Board.randomMaze(20, 20, false, startX, startY, endX, endY);
            Result result = new AStarAlgorithm(board).startSearch(board.getNode(startX, startY), board.getNode(endX, endY));
            if(result.path != null && result.path.size() >= 50){
                longResult = result;
                board.print(longResult, false);
                break;
            }
        }

    }

    private static void runObstacleBoardTest(){

        List<Result> results = new ArrayList<>();

        int sizeX = 30;
        int sizeY = 15;

        for(int i = 0; i < 50; i++){
            int startX = (int) (Math.random() * sizeX);
            int startY = (int) (Math.random() * sizeY);
            int endX = (int) (Math.random() * sizeX);
            int endY = (int) (Math.random() * sizeY);
            Board board = Board.randomObstacles(sizeX, sizeY, false, 0.35);
            Result r = new AStarAlgorithm(board).startSearch(board.getNode(startX, startY), board.getNode(endX, endY));
            results.add(r);
            if(r.path != null)
                board.print(r, false);
        }

        int failed = 0;
        long averageTime = 0;
        long averageTimeFailed = 0;
        long averageTimeSucceeded = 0;
        for(Result r : results){
            averageTime += r.timeInNano;
            if(r.failed()){
                failed++;
                averageTimeFailed += r.timeInNano;
            }else{
                averageTimeSucceeded += r.timeInNano;
            }
        }


        System.out.println();
        System.out.println("Average time: " + ((averageTime / (double) results.size()) / (1000000D)) + "ms");
        System.out.println("Average time failed [" + failed + "]: " + ((averageTimeFailed / (double) failed) / (1000000D)) + "ms");
        System.out.println("Average time succeeded [" + (results.size() - failed) + "]: " + ((averageTimeSucceeded / (double) (results.size() - failed)) / (1000000D)) + "ms");
    }

    private static void runInitialTest(){
        Board board = Board.emptyBoard(10, 10, false);
        List<Result> results = new ArrayList<>();

        for(int i = 0; i < 100; i++){
            Result r = new AStarAlgorithm(board).startSearch(board.getNode(0, 0), board.getNode(9, 8));
            results.add(r);
            board.reset();
        }

        results.remove(0);

        long averageTime = Util.getAverageTime(results);    // FIXME: This is not the average

        System.out.println();
        System.out.println("Average time: " + ((averageTime / (double) results.size()) / (1000000D)) + "ms");
    }

    public static File getExecutionPath(){
        return new File(System.getProperty("user.dir"));
    }
}
